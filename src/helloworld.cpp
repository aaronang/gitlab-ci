#include <iostream>
#include "boost/filesystem.hpp"
#include "vtkMath.h"

int main() {
    std::cout << "Hello, World!" << std::endl;
    std::cout << boost::filesystem::current_path() << std::endl;

    double p[3] = {0.0, 0.0, 0.0};
    double q[3] = {1.0, 1.0, 1.0};
    double squared_distance = vtkMath::Distance2BetweenPoints(p, q);

    std::cout << "Distance between P and Q: " << squared_distance << std::endl;
}
