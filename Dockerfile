FROM ubuntu:18.04

RUN apt-get update \
    && apt-get install --yes --no-install-recommends \
       ca-certificates wget cmake build-essential \
       libboost-all-dev \
       libgl1-mesa-dev libxt-dev \
    && rm -rf /var/lib/apt/lists/*

RUN wget https://gitlab.kitware.com/vtk/vtk/-/archive/v8.1.1/vtk-v8.1.1.tar.gz \
    && tar xf vtk-v8.1.1.tar.gz \
    && cmake -Hvtk-v8.1.1 -Bvtk-v8.1.1/build \
             -DCMAKE_BUILD_TYPE=Release \
             -DBUILD_SHARED_LIBS=ON \
             -DBUILD_DOCUMENTATION=OFF \
             -DBUILD_EXAMPLES=OFF \
             -DBUILD_TESTING=OFF \
    && make -j8 -C vtk-v8.1.1/build install \
    && rm -rf vtk-v8.1.1.tar.gz vtk-v8.1.1
